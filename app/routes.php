<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use RW\Middleware\AuthMiddleware;


$app->get('/login', 'AuthController:getLogin')->setName('auth.login');
$app->post('/login', 'AuthController:postLogin');
$app->get('/about', 'HomeController:getAbout')->setName('about');


$app->group('', function() {
  $this->get('/', 'HomeController:getIndex')->setName('home');

  $this->get('/user/register', 'AuthController:getRegister')->setName('auth.register');
  $this->post('/user/register', 'AuthController:postRegister');
  $this->get('/user/list', 'AuthController:getList')->setName('auth.list');
  $this->get('/user/edit', 'AuthController:getUser')->setName('auth.edit');
  $this->post('/user/edit', 'AuthController:postUser')->setName('auth.edit');
  $this->get('/user/logout', 'AuthController:getLogout')->setName('auth.logout');
  $this->get('/user/delete', 'AuthController:getDelete')->setName('auth.delete');
  $this->get('/user/changepwd', 'AuthController:getChangePwd')->setName('auth.change_pwd');
  $this->post('/user/changepwd', 'AuthController:postChangePwd');

  $this->get('/workshop/index', 'WorkshopController:getIndex')->setName('workshop.index');
  $this->get('/workshop/add', 'WorkshopController:getAdd')->setName('workshop.add');
  $this->post('/workshop/add', 'WorkshopController:postAdd');
  $this->get('/workshop/edit', 'WorkshopController:getEdit')->setName('workshop.edit');
  $this->post('/workshop/edit', 'WorkshopController:postEdit');
  $this->get('/workshop/json', 'WorkshopController:getJson')->setName('workshop.json');
  $this->post('/workshop/set_active', 'WorkshopController:postSetActive')->setName('workshop.setActive');
  $this->get('/workshop/close', 'WorkshopController:getClose')->setName('workshop.close');
  $this->get('/workshop/open', 'WorkshopController:getOpen')->setName('workshop.open');
  $this->get('/workshop/delete', 'WorkshopController:getDelete')->setName('workshop.delete');

  $this->get('/entries/index', 'EntryController:getIndex')->setName('entry.index');
  $this->get('/entries/json/index', 'EntryController:getEntries')->setName('entry.json.index');
  $this->get('/entries/json/get', 'EntryController:getEntry')->setName('entry.json.get');
  $this->post('/entries/ajax/send', 'EntryController:postEntry')->setName('entry.ajax.send');
  $this->get('/entries/delete', 'EntryController:getDelete')->setName('entry.delete');
  $this->get('/entries/report/full', 'EntryController:getReport')->setName('entry.report.full');
  $this->get('/entries/report/short', 'EntryController:getShortReport')->setName('entry.report.short');
  $this->get('/entries/record/get', 'EntryController:getRecord')->setName('entry.record');
  $this->get('/entries/record/delete', 'EntryController:getDelRecord')->setName('entry.record.delete');
  $this->get('/entries/csv', 'EntryController:getCSV')->setName('entry.report.csv');

  $this->get('/category/index', 'CategoryController:getIndex')->setName('category.index');
  $this->get('/category/add', 'CategoryController:getAdd')->setName('category.add');
  $this->post('/category/add', 'CategoryController:postAdd')->setName('category.add');
  $this->get('/category/edit', 'CategoryController:getEdit')->setName('category.edit');
  $this->post('/category/edit', 'CategoryController:postEdit');

})->add(new AuthMiddleware($container));
