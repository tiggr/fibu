<?php

namespace RW\Models;

use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{
  protected $fillable = [
    'name',
    'year',
    'image',
    'status'
  ];

}
