<?php

namespace RW\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
  protected $fillable = [
    'login',
    'password',
    'name',
    'email',
    'last_login'
  ];



  
}
