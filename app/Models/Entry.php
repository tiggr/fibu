<?php

namespace RW\Models;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
  protected $fillable = [
    'date',
    'title',
    'title2',
    'contact',
    'income',
    'expense',
    'workshop',
    'category',
    'user',
    'record'
  ];


  public function cat() {
    return $this->BelongsTo(Category::class, 'category', 'id');
  }

}
