<?php

namespace RW\Controllers;

use RW\Models\User;
use RW\Models\Workshop;
use RW\Models\Entry;

class HomeController extends Controller
{

  public function getIndex($request, $response) {

    $workshop = Workshop::where('id', $_SESSION['workshop'])->first();
    $expense = Entry::where([['workshop', '=', $_SESSION['workshop']], ['expense', '>', 0]])->sum('expense');
    $income = Entry::where([['workshop', '=', $_SESSION['workshop']], ['income', '>', 0]])->sum('income');
    $delta = $income - $expense;

    $entries = Entry::orderBy('date', 'desc')->take(5)->where('workshop', $_SESSION['workshop'])->get();

    foreach ($entries as $k => $e) {
      $entries[$k]['category_id'] = $e['category'];
      $entries[$k]['category'] = $e->cat['name'];
    };

    $view = $this->view->render($response, 'templates/home/home.twig', [
      'workshop' => $workshop,
      'sums' => ['income'=>$income, 'expense'=>$expense, 'delta'=>$delta],
      'bars' => ['income'=> max([$income, $expense]) > 0 ? $income/max([$income, $expense]) : 0,
                 'expense'=> max([$income, $expense]) > 0 ? $expense/max([$income, $expense]) : 0 ],
      'entries'=>$entries
    ]);

    return $view;
  }


  public function getAbout($request, $response)
  {
    return $this->view->render($response, 'templates/home/about.twig');
  }


}
