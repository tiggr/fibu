<?php

namespace RW\Controllers;

use Respect\Validation\Validator as v;
use Slim\Http\UploadedFile;

use RW\Models\User;
use RW\Models\Workshop;
use RW\Models\Entry;
use RW\Models\Category;

class EntryController extends Controller
{

  public function getIndex($request, $response) {

    $entries = Entry::orderBy('date', 'desc')->where('workshop', $_SESSION['workshop'])->get();
    $categories = Category::orderBy('name', 'asc')->get();

    $view = $this->view->render($response, 'templates/entries/index.twig', [
      'entries' => $entries,
      'categories' => $categories
    ]);
    return $view;
  }


  public function getEntry($request, $response) {

    $entry = Entry::where('id', $request->getParam('id'))->first();
    return $response->withJson($entry);
  }


  public function postEntry($request, $response) {

    $validation = $this->validator->validate($request, [
      'date' => v::notEmpty()->date(),
      'title' => v::notEmpty(),
      'contact' => v::notEmpty(),
      'category' => v::notEmpty(),
      'income' => v::optional(v::numeric()->positive()),
      'expense' => v::optional(v::numeric()->positive()),
    ]);

    if ($validation->failed()) {
      return $response->withStatus(500);
    }

    $uploadedFiles = $request->getUploadedFiles();

    $filename = NULL;
    if (!empty($uploadedFiles)) {
      $uploadedFile = $uploadedFiles['record'];
      $filename = $uploadedFile->getClientFilename();

      // check if file allready exists!
      $count = 0;
      $parts = pathinfo($filename);
      while (file_exists($this->container['settings']['dirs']['records'].$filename)) {
        $filename = $parts['filename']."($count).".$parts['extension'];
        $count++;
      }

      if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
          $uploadedFile->moveTo($this->container['settings']['dirs']['records'].$filename);
      }
    }

    if ($request->getParam('id')) {
      // id given -> update record
      Entry::where('id', $request->getParam('id'))->update([
        'date' => $request->getParam('date'),
        'title' => $request->getParam('title'),
        'title2' => $request->getParam('title2'),
        'contact' => $request->getParam('contact'),
        'income' => $request->getParam('income') ? $request->getParam('income') : 0,
        'expense' => $request->getParam('expense') ? $request->getParam('expense') : 0,
        'category' => $request->getParam('category'),
        'workshop' => $_SESSION['workshop'],
        'user' => $_SESSION['user'],
      ]);

      if ($filename != NULL) {
        $oldFile = Entry::where('id', $request->getParam('id'))->first()['record'];

        Entry::where('id', $request->getParam('id'))->update([
          'record' => $filename
        ]);

        if ($oldFile != '') { unlink($this->container['settings']['dirs']['records'].$oldFile); }
      }

      return $response->withStatus(200);
    } else {
      // no id given -> create record
      Entry::create([
        'date' => $request->getParam('date'),
        'title' => $request->getParam('title'),
        'title2' => $request->getParam('title2'),
        'contact' => $request->getParam('contact'),
        'income' => $request->getParam('income') ? $request->getParam('income') : 0,
        'expense' => $request->getParam('expense') ? $request->getParam('expense') : 0,
        'category' => $request->getParam('category'),
        'workshop' => $_SESSION['workshop'],
        'user' => $_SESSION['user'],
        'record' => $filename,
      ]);

      return $response->withStatus(200);
    }
  }


  public function getEntries($request, $response) {

    $entries = Entry::where('workshop', $_SESSION['workshop'])->get();

    foreach ($entries as $k => $e) {
      $entries[$k]['category_id'] = $e['category'];
      $entries[$k]['category'] = $e->cat['name'];
    };

    return $response->withJson(['data' => $entries]);
  }


  public function getDelete($request, $response) {
    $entry = Entry::where('id', $request->getParam('id'))->first();
    $title = $entry['title'];
    $entry->delete();

    $this->flash->addMessage('success', 'Der Datensatz "'.$title.'" wurde erfolgreich gelöscht.');
    return $response->withRedirect($this->router->pathFor('entry.index'));
  }


  public function getReport($request, $response) {

    $workshop = Workshop::find($_SESSION['workshop']);

    $report = $this->reportData($_SESSION['workshop']);

    $view = $this->view->render($response, 'templates/entries/report_long.twig', [
      'report' => ['income' => $report['income'], 'expense' => $report['expense']],
      'sum' => ['income' => $report['sum_income'],
                'expense' => $report['sum_expense'],
                'sum' => $report['sum_income']-$report['sum_expense']],
      'workshop' => $workshop,
    ]);
    return $view;
  }


  public function getShortReport($request, $response) {

    $workshop = Workshop::find($_SESSION['workshop']);

    $report = $this->reportData($_SESSION['workshop']);

    $view = $this->view->render($response, 'templates/entries/report_short.twig', [
      'report' => ['income' => $report['income'], 'expense' => $report['expense']],
      'sum' => ['income' => $report['sum_income'],
                'expense' => $report['sum_expense'],
                'sum' => $report['sum_income']-$report['sum_expense']],
      'workshop' => $workshop,
    ]);
    return $view;
  }




  private function reportData($id) {
    $income = Entry::orderBy('date', 'asc')->where('workshop', $id)->where('income', '>', 0)->get();

    foreach ($income as $k => $e) {
      $income[$k]['category_id'] = $e['category'];
      $income[$k]['category'] = $e->cat['name'];
    };

    $grp_income = $income->groupBy('category');
    $sum_income = $income->sum('income');

    foreach ($grp_income as $key => $val) {
      $entries = [];
      $sum = 0;
      foreach ($val as $entry) {
        $sum += $entry['income'];
        $entries[] = $entry;
      };
      $grp_in[$key]['entries'] = $entries;
      $grp_in[$key]['sum'] = $sum;
    }

    $expense = Entry::orderBy('date', 'asc')->where('workshop', $_SESSION['workshop'])->where('expense', '>', 0)->get();

    foreach ($expense as $k => $e) {
      $expense[$k]['category_id'] = $e['category'];
      $expense[$k]['category'] = $e->cat['name'];
    };

    $grp_expense = $expense->groupBy('category');
    $sum_expense = $expense->sum('expense');

    foreach ($grp_expense as $key => $val) {
      $entries = [];
      $sum = 0;
      foreach ($val as $entry) {
        $sum += $entry['expense'];
        $entries[] = $entry;
      };
      $grp_ex[$key]['sum'] = $sum;
      $grp_ex[$key]['entries'] = $entries;
    }
    return ['income' => $grp_in, 'expense' => $grp_ex, 'sum_income' => $sum_income, 'sum_expense' => $sum_expense];
  }


  public function getRecord($request, $response) {

    $record = Entry::where('id', $request->getParam('id'))->first();


    $file = $this->container['settings']['dirs']['records'].$record['record'];
    if (file_exists($file)) {
      $response = $response->withHeader('Content-Description', 'File Transfer')
      ->withHeader('Content-Type', mime_content_type($file))
      ->withHeader('Content-Disposition', 'inline;filename="'.basename($file).'"')
      ->withHeader('Expires', '0')
      ->withHeader('Cache-Control', 'must-revalidate')
      ->withHeader('Pragma', 'public')
      ->withHeader('Content-Length', filesize($file));

      readfile($file);
      return $response;
    } else {
      return $response->withStatus(404);
    }
  }


  public function getDelRecord($request, $response) {
    $record = Entry::where('id', $request->getParam('id'))->first();

    unlink($this->container['settings']['dirs']['records'].$record['record']);

    Entry::where('id', $request->getParam('id'))->update(['record' => null]);

    return $response->withRedirect($this->router->pathFor('entry.index'));
  }


  public function getCSV($request, $response) {
    $fields = array('id', 'date', 'title', 'title2', 'income', 'expense', 'contact', 'category');
    $sep = $this->container['settings']['csv']['separator'];
    $string = $this->container['settings']['csv']['string'];
    $lb = $this->container['settings']['csv']['linebreak'];
    $dec = $this->container['settings']['csv']['decimal'];

    $entries = Entry::where('workshop', $_SESSION['workshop'])->get();
    $workshop = Workshop::find($_SESSION['workshop']);

    $body = implode($sep, $fields) . $lb;

    foreach ($entries as $k => $e) {
      $entries[$k]['category_id'] = $e['category'];
      $entries[$k]['category'] = $e->cat['name'];
      
      $line = array(); 
      foreach ($fields as $i) {
        if (is_numeric($entries[$k][$i])) { 
          if ($entries[$k][$i] == 0) { $line[] = ''; }
          else { $line[] = number_format($entries[$k][$i], 2, $dec, ''); }
        }
        else { $line[] = $string . $entries[$k][$i] . $string; }
      }
      $body = $body . implode($sep, $line) . $lb;
    };

    return $response->withHeader('Content-Type', 'text/csv')
                    ->withHeader('Content-Disposition', 'attachment;filename=fibu_export_'.$workshop['name'].'.csv')
                    ->withHeader('Expires', '0')
                    ->withHeader('Cache-Control', 'must-revalidate')
                    ->withHeader('Pragma', 'public')
                    ->withHeader('Content-Length', strlen($body))
                    ->write($body);
  }



}
