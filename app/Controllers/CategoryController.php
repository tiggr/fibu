<?php

namespace RW\Controllers;

use Respect\Validation\Validator as v;
use RW\Models\Category;

class CategoryController extends Controller
{

  public function getIndex($request, $response) {

    $categories = Category::all();

    return $this->view->render($response, 'templates/categories/index.twig', ['categories'=> $categories]);;
  }


  public function getAdd($request, $response) {

    return $this->view->render($response, 'templates/categories/add.twig');;
  }


  public function postAdd($request, $response) {

    $validation = $this->validator->validate($request, [
      'name' => v::notEmpty(),
    ]);

    if ($validation->failed()) {
      return $response->withRedirect($this->router->pathFor('category.add'));
    }

    $user = Category::create([
      'name' => $request->getParam('name'),
    ]);

    $this->flash->addMessage('success', 'Die Kategorie "'.$request->getParam('name').'" wurde erfolgreich angelegt.');
    return $response->withRedirect($this->router->pathFor('category.index'));
  }


  public function getEdit($request, $response) {

    $category = Category::where('id', $request->getParam('id'))->first();

    return $this->view->render($response, 'templates/categories/edit.twig', ['category' => $category]);
  }


  public function postEdit($request, $response) {

    $validation = $this->validator->validate($request, [
      'name' => v::notEmpty(),
    ]);

    if ($validation->failed()) {
      return $response->withRedirect($this->router->pathFor('category.edit'));
    }

    Category::where('id', $request->getParam('id'))->update([
      'name'=>$request->getParam('name'),
    ]);

    $this->flash->addMessage('success', 'Die Kategorie "'.$request->getParam('name').'" wurde erfolgreich geändert.');
    return $response->withRedirect($this->router->pathFor('category.index'));
  }


  public function getJson($request, $response) {

    $workshops = Workshop::orderBy('year', 'desc')->get();

    return $response->withJson($workshops);
  }


  public function postSetActive($request, $response) {

    $_SESSION['workshop'] = $request->getParam('id');

    return 'OK';
  }


  public function getClose($request, $response) {

    Workshop::where('id', $request->getParam('id'))->update([
      'status' => 'closed'
    ]);

    return $response->withRedirect($this->router->pathFor('workshop.index'));
  }


  public function getOpen($request, $response) {

    Workshop::where('id', $request->getParam('id'))->update([
      'status' => 'open'
    ]);

    return $response->withRedirect($this->router->pathFor('workshop.index'));
  }


  public function getDelete($request, $response) {

    $count = Entry::where('workshop', $request->getParam('id'))->count();
    $workshop = Workshop::where('id', $request->getParam('id'))->first();

    if ($count > 0) {
      $this->flash->addMessage('error', 'Der Workshop "'.$workshop['name'].'" konnte nicht gelöscht werden, da Buchungen vorhanden sind.');
    } else {
      $this->flash->addMessage('success', 'Der Workshop "'.$workshop['name'].'" wurde erfolgreich gelöscht.');
      $workshop->delete();
    }
    return $response->withRedirect($this->router->pathFor('workshop.index'));
  }


}
