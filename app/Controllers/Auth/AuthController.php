<?php

namespace RW\Controllers\Auth;

use RW\Controllers\Controller;
use Respect\Validation\Validator as v;
use RW\Models\User;
use RW\Models\Workshop;

class AuthController extends Controller
{


  public function getLogin($request, $response) {
    return $this->view->render($response, 'templates/auth/login.twig');
  }


  public function postLogin($request, $response) {

    $auth = $this->auth->attempt(
      $request->getParam('login'),
      $request->getParam('password')
    );

    if (!$auth) {
      $this->flash->addMessage('error', 'Die Anmeldung ist fehlgeschlagen!');
      return $response->withRedirect($this->router->pathFor('auth.login'));
    }

    $this->flash->addMessage('success', 'Du hast Dich erfolgreich angemeldet!');

    User::where('login', $request->getParam('login'))->update([
      'last_login' => date('Y-m-d H:i:s')
    ]);

    $_SESSION['workshop'] = Workshop::orderBy('year', 'desc')->first()->id;

    return $response->withRedirect($this->router->pathFor('home'));
  }


  public function getLogout($request, $response) {

    $this->auth->logout();

    return $response->withRedirect($this->router->pathFor('home'));
  }


  public function postRegister($request, $response) {

    $validation = $this->validator->validate($request, [
      'email' => v::email()->notEmpty(),
      'name' => v::notEmpty(),
      'login' => v::noWhitespace()->notEmpty()->alnum()->uniqueLogin(),
      'password' => v::noWhitespace()->notEmpty()->length(6, null)
    ]);

    if ($validation->failed()) {
      return $response->withRedirect($this->router->pathFor('auth.register'));
    }

    $user = User::create([
      'email' => $request->getParam('email'),
      'name' => $request->getParam('name'),
      'login' => $request->getParam('login'),
      'password' => password_hash($request->getParam('password'), PASSWORD_DEFAULT)
    ]);

    $this->flash->addMessage('success', 'Der User "'.$request->getParam('name').'" wurde erfolgreich angelegt.');
    return $response->withRedirect($this->router->pathFor('auth.list'));
  }

  public function getRegister($request, $response) {

    return $this->view->render($response, 'templates/auth/register.twig');
  }


  public function getList($request, $response) {

    $users = User::all();

    return $this->view->render($response, 'templates/auth/users.twig', ['users' => $users]);
  }


  public function getUser($request, $response) {
    $user = User::where('id', $request->getParam('id'))->first();

    return $this->view->render($response, 'templates/auth/edit.twig', ['user' => $user]);
  }


  public function postUser($request, $response) {

    $validation = $this->validator->validate($request, [
      'email' => v::email()->notEmpty(),
      'name' => v::notEmpty(),
    ]);

    if ($validation->failed()) {
      return $response->withRedirect($this->router->pathFor('auth.edit'));
    }

    User::where('id', $request->getParam('id'))->update([
      'email'=>$request->getParam('email'),
      'name'=>$request->getParam('name'),
    ]);

    $this->flash->addMessage('success', 'Der User "'.$request->getParam('name').' ('.$request->getParam('login').')" wurde erfolgreich geändert.');
    return $response->withRedirect($this->router->pathFor('auth.list'));
  }


  public function getDelete($request, $response) {
    $user = User::where('id', $request->getParam('id'))->first();
    $name = $user['name'].' ('.$user['login'].')';
    $user->delete();

    $this->flash->addMessage('success', 'Der Benutzer "'.$name.'" wurde erfolgreich gelöscht.');
    return $response->withRedirect($this->router->pathFor('auth.list'));
  }


  public function postChangePwd($request, $response) {

    $validation = $this->validator->validate($request, [
      'password' => v::noWhitespace()->notEmpty()->length(6, null)
    ]);

    if ($validation->failed()) {
      return $response->withRedirect($this->router->pathFor('auth.change_pwd'));
    }

    $user = User::where('id', $request->getParam('id'))->update([
      'password' => password_hash($request->getParam('password'), PASSWORD_DEFAULT)
    ]);

    $this->flash->addMessage('success', 'Das Passwort wurde erfolgreich geändert.');
    return $response->withRedirect($this->router->pathFor('auth.list'));
  }


  public function getChangePwd($request, $response) {
    return $this->view->render($response, 'templates/auth/change_pwd.twig', ['id'=>$request->getParam('id')]);
  }

}
