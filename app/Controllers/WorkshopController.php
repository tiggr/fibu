<?php

namespace RW\Controllers;

use Respect\Validation\Validator as v;
use RW\Models\Workshop;
use RW\Models\Entry;

class WorkshopController extends Controller
{

  public function getIndex($request, $response) {

    $workshops = Workshop::all();

    return $this->view->render($response, 'templates/workshops/index.twig', ['workshops'=> $workshops]);;
  }


  public function getAdd($request, $response) {

    return $this->view->render($response, 'templates/workshops/add.twig');;
  }


  public function postAdd($request, $response) {

    $validation = $this->validator->validate($request, [
      'name' => v::notEmpty(),
      'year' => v::intVal()->between(1970,2100),
    ]);

    if ($validation->failed()) {
      return $response->withRedirect($this->router->pathFor('workshop.add'));
    }

    $user = Workshop::create([
      'name' => $request->getParam('name'),
      'year' => $request->getParam('year'),
    ]);

    $this->flash->addMessage('success', 'Der Workshop "'.$request->getParam('name').'" wurde erfolgreich angelegt.');
    return $response->withRedirect($this->router->pathFor('workshop.index'));
  }


  public function getEdit($request, $response) {

    $workshop = Workshop::where('id', $request->getParam('id'))->first();

    return $this->view->render($response, 'templates/workshops/edit.twig', ['workshop' => $workshop]);
  }


  public function postEdit($request, $response) {

    $validation = $this->validator->validate($request, [
      'name' => v::notEmpty(),
      'year' => v::intVal()->between(1970,2100),
    ]);

    if ($validation->failed()) {
      return $response->withRedirect($this->router->pathFor('workshop.edit'));
    }

    Workshop::where('id', $request->getParam('id'))->update([
      'name'=>$request->getParam('name'),
      'year'=>$request->getParam('year')
    ]);

    $this->flash->addMessage('success', 'Der Workshop "'.$request->getParam('name').'" wurde erfolgreich geändert.');
    return $response->withRedirect($this->router->pathFor('workshop.index'));
  }


  public function getJson($request, $response) {

    $workshops = Workshop::orderBy('year', 'desc')->get();

    return $response->withJson($workshops);
  }


  public function postSetActive($request, $response) {

    $_SESSION['workshop'] = $request->getParam('id');

    return 'OK';
  }


  public function getClose($request, $response) {

    Workshop::where('id', $request->getParam('id'))->update([
      'status' => 'closed'
    ]);

    return $response->withRedirect($this->router->pathFor('workshop.index'));
  }


  public function getOpen($request, $response) {

    Workshop::where('id', $request->getParam('id'))->update([
      'status' => 'open'
    ]);

    return $response->withRedirect($this->router->pathFor('workshop.index'));
  }


  public function getDelete($request, $response) {

    $count = Entry::where('workshop', $request->getParam('id'))->count();
    $workshop = Workshop::where('id', $request->getParam('id'))->first();

    if ($count > 0) {
      $this->flash->addMessage('error', 'Der Workshop "'.$workshop['name'].'" konnte nicht gelöscht werden, da Buchungen vorhanden sind.');
    } else {
      $this->flash->addMessage('success', 'Der Workshop "'.$workshop['name'].'" wurde erfolgreich gelöscht.');
      $workshop->delete();
    }
    return $response->withRedirect($this->router->pathFor('workshop.index'));
  }


}
