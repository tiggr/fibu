<?php

namespace RW\Validation\Exceptions;

use Respect\Validation\Exceptions\ValidationException;

class UniqueLoginException extends ValidationException
{
  public static $defaultTemplates = [
    self::MODE_DEFAULT => [
      self::STANDARD => 'the login is allready in use'
    ]
  ];
}
