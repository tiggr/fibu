<?php

namespace RW\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;
use RW\Models\User;

class UniqueLogin extends AbstractRule
{
  public function validate($input) {
    return User::where('login', $input)->count() === 0;
  }
}
