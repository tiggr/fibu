<?php

namespace RW\Auth;

use RW\Models\User;

class Auth
{
  public function attempt($login, $password) {
    $user = User::where('login', $login)->first();

    if (!$user) { return false; }

    if (password_verify($password, $user->password)) {
      $_SESSION['user'] = $user->id;
      return true;
    }

    return false;
  }


  public function check() {
    return isset($_SESSION['user']);
  }

  public function user() {
    return User::where('id', $_SESSION['user'])->first();
  }


  public function logout() {

    unset($_SESSION['user']);
  }

}
