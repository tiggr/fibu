<?php

namespace RW\Middleware;

class ValidationErrorsMiddleware extends Middleware
{
  public function __invoke($request, $response, $next) {

    if (array_key_exists('validation', $_SESSION)) {
      $this->container->view->getEnvironment()->addGlobal('validation', $_SESSION['validation']);
      unset($_SESSION['validation']);
    }
    
    $response = $next($request, $response);
    return $response;
  }



}
