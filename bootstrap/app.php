<?php

session_start();

use Respect\Validation\Validator as v;

require '../vendor/autoload.php';
require 'config.php';

$app = new \Slim\App(['settings' => $config]);

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function($container) use ($capsule) {
  return $capsule;
};

$container['auth'] = function ($container) { return new \RW\Auth\Auth; };

$container['view'] = function($container) {
    $view = new \Slim\Views\Twig('../resources/views', ['cache' => $container['settings']['dirs']['twigcache']]);

    $view->addExtension(new \Slim\Views\TwigExtension(
      $container->router,
      $container->request->getUri()
    ));

    $view->getEnvironment()->addGlobal('auth', [
      'check' => $container->auth->check(),
      'user' => $container->auth->user()
    ]);

    $view->getEnvironment()->addGlobal('flash', $container->flash);
    $view->getEnvironment()->addGlobal('fibuversion', $container['settings']['version']);
    $view->getEnvironment()->addGlobal('session', $_SESSION);

    return $view;
};

$container['validator'] = function ($container) { return new \RW\Validation\Validator; };
$container['HomeController'] = function ($container) { return new \RW\Controllers\HomeController($container); };
$container['AuthController'] = function ($container) { return new \RW\Controllers\Auth\AuthController($container); };
$container['WorkshopController'] = function ($container) { return new \RW\Controllers\WorkshopController($container); };
$container['EntryController'] = function ($container) { return new \RW\Controllers\EntryController($container); };
$container['CategoryController'] = function ($container) { return new \RW\Controllers\CategoryController($container); };
$container['flash'] = function ($container) { return new \Slim\Flash\Messages(); };

$app->add(new RW\Middleware\ValidationErrorsMiddleware($container));
$app->add(new RW\Middleware\OldInputMiddleware($container));

v::with('RW\\Validation\\Rules\\');

require '../app/routes.php';
