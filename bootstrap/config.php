<?php

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$config['version'] = 'V0.22&alpha;';

$config['db']['driver'] = 'mysql';
$config['db']['host']   = 'localhost';
$config['db']['username']   = '';
$config['db']['password']   = '';
$config['db']['database'] = '';
$config['db']['charset'] = 'utf8';
$config['db']['collation'] = 'utf8_unicode_ci';
$config['db']['prefix'] = '';

// use trailing slashes at paths
$config['dirs']['records'] = '/home/rodgauer_workshop_de/fibu_belege/';
$config['dirs']['twigcache'] = '/home/rodgauer_workshop_de/www/subdomains/fibu/cache/'; // false

$config['csv']['separator'] = ';';
$config['csv']['string'] = '"';
$config['csv']['decimal'] = ',';
$config['csv']['linebreak'] = "\r\n";
