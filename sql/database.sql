CREATE TABLE IF NOT EXISTS users (
  id INT AUTO_INCREMENT NOT NULL,
  login VARCHAR(256) NOT NULL UNIQUE,
  password VARCHAR(256),
  name VARCHAR(256),
  email VARCHAR(256),
  last_login TIMESTAMP,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) CHARACTER SET = 'utf8' COLLATE = 'utf8_unicode_ci';
INSERT INTO users (login, name, email)
VALUES
  (
    'admin',
    'Administrator',
    'info@rodgauer-workshop.de'
  );
INSERT INTO users (login, name, email)
VALUES
  (
    'tiggr',
    'Marcus J. Ertl',
    'marcus.ertl@rodgauer-workshop.de'
  );
CREATE TABLE IF NOT EXISTS workshops (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(256) NOT NULL,
    year SMALLINT NOT NULL,
    image VARCHAR(256),
    status VARCHAR(32) NOT NULL DEFAULT 'open';
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
  ) CHARACTER SET = 'utf8' COLLATE = 'utf8_unicode_ci';
INSERT INTO workshops (name, year)
VALUES
  ('Toront', 2017);
INSERT INTO workshops (name, year)
VALUES
  ('Faltstern', 2018);
CREATE TABLE IF NOT EXISTS entries (
    id INT AUTO_INCREMENT NOT NULL,
    date DATE NOT NULL,
    title VARCHAR(256) NOT NULL,
    title2 VARCHAR(256) DEFAULT NULL,
    contact VARCHAR(256) NOT NULL,
    income DECIMAL(15, 2) NOT NULL DEFAULT 0,
    expense DECIMAL(15, 2) NOT NULL DEFAULT 0,
    workshop INT NOT NULL,
    category VARCHAR(256),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    user INT NOT NULL,
    PRIMARY KEY (id),
    INDEX (workshop)
  ) CHARACTER SET = 'utf8' COLLATE = 'utf8_unicode_ci';
CREATE TABLE IF NOT EXISTS categories (
    id INT AUTO_INCREMENT NOT NULL,
    name VARCHAR(256),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
  ) CHARACTER SET = 'utf8' COLLATE = 'utf8_unicode_ci';